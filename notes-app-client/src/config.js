export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    s3: {
      REGION: "us-east-1",
      BUCKET: "jad-sstutorial-note-upload"
    },
    apiGateway: {
      REGION: "us-east-1",
      URL: "https://rupicdiphb.execute-api.us-east-1.amazonaws.com/prod"
    },
    cognito: {
      REGION: "us-east-1",
      USER_POOL_ID: "us-east-1_cOwY94pAZ",
      APP_CLIENT_ID: "6g0dg1evpvarklfl9ja0r2ctpj",
      IDENTITY_POOL_ID: "us-east-1:dd764870-8d90-4663-afbc-79508e1d07f7"
    }
  };
  